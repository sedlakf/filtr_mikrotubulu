function drawMTTracksRes(projDir)
    [xcoords, ycoords, center, directionCorrelation] = filterMTTracks(projDir);

    rmiOut = csvread([projDir filesep "rmi_out.csv"]);
    labels = rmiOut(2:end,3); % We need to ignore the header and select only the label column

    figure
    hold all;

%    set(gca, 'ColorOrder', jet(nTracks))


    % plot(xs', ys');
    plot(center(1), center(2), 'g*');
    % colorbar

    nTracks = size(xcoords, 2)
    size(labels)

    lblCols = cell(nTracks, 1);
    assert(size(labels, 1) == nTracks);

    for i=1:nTracks
        lblCols{i} = 'k';
        if labels(i) == 0
            lblCols{i} = 'r';
        elseif labels(i) == 1
            lblCols{i} = 'b';
        end
    end

    for i=1:nTracks

        plot(xcoords(:,i), ycoords(:, i), lblCols(i));

        firstIdx = find(~isnan(xcoords(:, i)), 1, 'first');
        text(xcoords(firstIdx,i), ycoords(firstIdx,i), [num2str(i) ': ' num2str(directionCorrelation(i))])
    end

    hold off;
end

