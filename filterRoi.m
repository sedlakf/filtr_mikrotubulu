function [csvname] = filterRoi(dir)
    projDir = [dir filesep "roi_1"];
    [xcoords, ycoords, center, dirCorr] = filterMTTracks(projDir);

    labels = csvread([projDir filesep "labels.csv"]);
    % drawMTTracks(xcoords, ycoords, center, dirCorr, labels);

    nTracks = size(xcoords, 2);
    trackLabels = nan(nTracks, 1);
    trackLabels(labels(:, 1)) = labels(:, 2);

    feats = csvread([projDir filesep "trackFeats.csv"]);

    header = {"distance", "directionCorrelation", "keep"};
    csvname = [projDir filesep "labeledTrackFeats.csv"];
    f = fopen(csvname, 'w');
    %Write the header:
    fprintf(f,'%-10s,',header{1:end-1});
    fprintf(f,'%-10s\n',header{end});fopen(csvname);
    fclose(csvname);
    csvwrite([projDir filesep "labeledTrackFeats.csv"], horzcat(feats, trackLabels), '-append')
end
