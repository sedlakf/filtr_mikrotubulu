function drawMTTracks(xcoords, ycoords, center, directionCorrelation, labels)
    figure
    hold all;

%    set(gca, 'ColorOrder', jet(nTracks))


    % plot(xs', ys');
    plot(center(1), center(2), 'g*');
    % colorbar

    nTracks = size(xcoords, 2)

    lblCols = cell(nTracks, 1);
    for i=1:nTracks
        labelIdx = find(labels(:, 1) == i);
        label = NaN;
        if size(labelIdx, 1) > 0
            label = labels(labelIdx(1), 2);
            if label == 0
                lblCols{i} = 'r';
            elseif label == 1
                lblCols{i} = 'b';
            end
        else 
            lblCols{i} = 'k';
        end
    end

    for i=1:nTracks

        plot(xcoords(:,i), ycoords(:, i), lblCols(i));

        firstIdx = find(~isnan(xcoords(:, i)), 1, 'first');
        text(xcoords(firstIdx,i), ycoords(firstIdx,i), [num2str(i) ': ' num2str(directionCorrelation(i))])
    end

    hold off;
end

