% projDir = 'REOproMatlab - Copy/2014-08-07-MC556-REO-after6xMSB-02_R3D_cmle-vyrez1/roi_1';

function [xcoords, ycoords, center, directionCorrelation] = filterMTTracks(projDir)
    
    load([projDir filesep 'meta' filesep 'projData']);

    % extract merged track info (block matrix) - take abs value and
    % convert displacement to microns and lifetimes to sec
    dataMatMerge=plusTipMergeSubtracks(projData); % merged data is first output
    allData=abs(dataMatMerge);
    allData(:,6)=allData(:,6).*projData.secPerFrame;
    allData(:,7)=allData(:,7).*(projData.pixSizeNm./1000);
    % get all the subtrack coordinates corresponding to the merged data
    [xMat,yMat]=plusTipGetSubtrackCoords(projData,[],1);

    yMat = -yMat;

    nTracks = size(xMat, 1);

    tracks = zeros([size(xMat) 2]);
    tracks(:,:,1) = xMat;
    tracks(:,:,2) = yMat;

    starts = zeros(nTracks,1);
    firstPoints = zeros(nTracks, 2);
    trackVectors = zeros(nTracks, 2);
    for i=1:nTracks
       starts(i) = find(~isnan(xMat(i,:)), 1, 'first');
       last = find(~isnan(xMat(i,:)), 1, 'last');
       track = tracks(i, starts(i):last, :);
       trSize = size(track);
       assert(trSize(1) == 1, 'the first dimension is useless')

       firstPoints(i,:) = track(:,1,:);

       trackVectors(i,:) = track(:,end,:) - track(:,1,:);

    end

    center = mean(firstPoints);

    % d = sqrt((xa-xb)**2 + (ya - yb)**2)
    distances = sqrt(sum(bsxfun(@minus, firstPoints, center) .^ 2, 2));

    expectedVectors = bsxfun(@minus, firstPoints, center);

    directionCorrelation = dot(normr(expectedVectors), normr(trackVectors), 2);

    csvwrite([projDir filesep "trackFeats.csv"], horzcat(distances, directionCorrelation))

    % only vectors
    lastPoints = firstPoints + trackVectors;
    xs = horzcat(firstPoints(:,1), lastPoints(:,1));
    ys = horzcat(firstPoints(:,2), lastPoints(:,2));

    xcoords = xMat';
    ycoords = yMat';

end
